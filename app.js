//bai1
var sum = 0;
var result = 0;
for (var i = 1; i < 10000; i++) {
    if (sum + i > 10000) {
        result = i;
        break;
    }
    sum += i;
}

document.getElementById("result_1").innerHTML = `
    <p class="alert-success p-3">Số nguyên dương nhỏ nhất: ${result}</p>
`;

//bai2 
function handleSum() {
    var sum2 = 0;
    var temp = 1;
    var n = document.getElementById("nNumber").value * 1;
    var x = document.getElementById("xNumber").value * 1;
    for (var i = 0; i < n; i++) {
        temp *= x;
        sum2 += temp;
    }

    document.getElementById("result_2").innerHTML = `
        <p>${sum2}</p>
    `
}

//bai3
function handlefactorial() {
    var n = document.getElementById("nNumber3").value * 1;
    var mul = 1;
    for (var i = 1; i <= n; i++) {
        mul *= i;
    }
    document.getElementById("result_3").innerHTML = `
        <p>${mul}</p>
    `
}

//bai4
function createDiv() {
    var str = "";
    for (var i = 1; i <= 10; i++) {
        if (i % 2 == 0) {
            str += `
            <div class="p-2 bg-danger text-white">Div Chẵn ${i}</div>
        `
        } else {
            str += `
            <div class="p-2 bg-primary text-white">Div Lẻ ${i}</div>
        `
        }
    }
    document.getElementById("wrapper").innerHTML = str;
}
